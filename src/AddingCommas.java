import acm.program.ConsoleProgram;

public class AddingCommas extends ConsoleProgram{

	public void run() {
		
		while (true) {
			String digits = readLine("Enter a numberic string: ");
			if (digits.length() == 0) break;
			println(addCommasToNumbericString(digits));
		}
	}
	
	private String addCommasToNumbericString(String digits) {
		int x = 0; // counting for every 3 numbers 
		// reverse the number order
		digits = reverseOrder(digits);
		
		String newDigits = "";
		for (int i = 0; i < digits.length(); i++) {
			char ch = digits.charAt(i);
			
			// after every 3 number it will put a ","
			if (x == 3) {
				newDigits += "," ;
				x = 0;
				}
			newDigits += ch;
			x++;
			}
		// reverse the order back to the original order
		newDigits = reverseOrder(newDigits);
		return newDigits;
	}
	
	// reverse the order so the number will start at the end
	private String reverseOrder(String digits) {
		String newDigits = "";
		for (int i = 0; i < digits.length(); i++) {
			char ch = digits.charAt(i);
			newDigits = ch + newDigits;
			
		}
		return newDigits;
		
	}

}
