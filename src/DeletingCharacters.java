import acm.program.ConsoleProgram;

public class DeletingCharacters extends ConsoleProgram{
	
	public String removeAllOccurrences(String str, char ch) {
		String replace = "";
		
		replace = str.replace(ch,' ');
		System.out.println(replace);
		return replace;
		
	}
	
	public void run(){
		removeAllOccurrences("This is a test" , 't');
		removeAllOccurrences("Summer is here!" , 'e');
		removeAllOccurrences("-----0----" , '-');
	}

}
